<?php

namespace Drupal\Tests\widget_provider_example\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests UI elements of the WidgetProviderApi Examples.
 *
 * @group widget_provider_example
 */
class WidgetExampleFunctionalTest extends BrowserTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'user',
    'node',
    'views',
    'widget_provider_api',
    'widget_provider_example',
  ];

  /**
   * Plain theme with no styling for better testing.
   *
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Permissions for the admin user.
   *
   * @var array
   */
  protected $adminPermissions = [
    'access widgets',
  ];

  /**
   * User with proper permissions for module configuration.
   *
   * @var \Drupal\user\Entity\User|false
   */
  protected $adminUser;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    // Gibt error, aber sonst Container error.
    parent::setUp();
    // Create an admin user.
    $this->adminUser = $this->drupalCreateUser($this->adminPermissions);
    $this->drupalLogin($this->adminUser);
  }

  /**
   * Tests to see if the Widget View Example url exists.
   */
  public function testUrlViewExampleExists() {
    $this->drupalGet('/widget/example-provider1/widget-view-example');
    $this->assertSession()->statusCodeEquals(200);
  }

  /**
   * Tests to see if the View is rendered on the site.
   */
  public function testViewExists() {
    $this->drupalGet('/widget/example-provider1/widget-view-example');
    $this->assertSession()->pageTextContains('No front page content has been created yet.');
  }

  /**
   * Tests to see if the Widget Node Example url exists.
   */
  public function testNodeExampleWorks() {
    // Create a test node type + node::
    $this->createContentType(['type' => 'article']);
    $node = $this->createNode(['type' => 'article']);
    // Check if we can access the node we exposed through our custom widget:
    $this->drupalGet('/widget/example-provider1/widget-node-example', [
      'query' => ['nid' => (string) $node->id(), 'kid' => '1'],
    ]);
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains($node->body->value);
  }

  /**
   * Tests to see if the Widget IFrame Example url exists.
   */
  public function testIframeExampleWorks() {
    $this->drupalGet('/widget/example-provider1/widget-iframe-example');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->elementAttributeExists('css', 'iframe', 'src');
    $this->assertSession()->elementAttributeContains('css', 'iframe', 'src', 'https://www.google.com/');
  }

  /**
   * Tests to see if the Widget Js Example url exists.
   */
  public function testJsonExampleWorks() {
    $this->drupalGet('/widget/example-provider2/widget-js-example', ['query' => ['kid' => '1', 'hid' => '1']]);
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->responseContains('This is an example from JS Example');
  }

  /**
   * Tests to see if there is no access, without the required parameter.
   */
  public function testJsonRequiredAccess() {
    $this->drupalGet('/widget/example-provider2/widget-js-example', ['query' => ['hid' => '1']]);
    $this->assertSession()->statusCodeEquals(404);
  }

  /**
   * Tests to see if there is access, even without the optional Parameter.
   */
  public function testJsonOptionalAccess() {
    $this->drupalGet('/widget/example-provider2/widget-js-example', ['query' => ['kid' => '1']]);
    $this->assertSession()->responseContains('{"data":"This is an example from JS Example"}');
  }

  /**
   * Tests if some random Widget doesn't exist.
   */
  public function testWidgetDoesNotExist() {
    $this->drupalGet('/widget/example-provider2/randomWidget');
    $this->assertSession()->statusCodeEquals(404);
  }

  /**
   * Tests to see if there is access to the widget page if logged out.
   */
  public function testAccessLoggedOut() {
    $this->drupalLogout();
    $this->drupalGet('/widget/example-provider1/widget-iframe-example');
    $this->assertSession()->statusCodeEquals(403);
  }

}
