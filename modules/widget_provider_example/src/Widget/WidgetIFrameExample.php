<?php

namespace Drupal\widget_provider_example\Widget;

use Drupal\widget_provider_api\Widget\WidgetBase;

/**
 * A Iframe Widget Example class.
 *
 * It renders an Iframe as a widget.
 */
class WidgetIFrameExample extends WidgetBase {

  /**
   * The WidgetIFrameExample Constructor.
   */
  public function __construct() {
    parent::__construct('widget-iframe-example');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    return $this->buildIframe('https://www.google.com/');
  }

  /**
   * {@inheritdoc}
   */
  public function buildContent(): array {
    return [];
  }

  /**
   * Function for rendering an iframe.
   *
   * @param string $url
   *   The URL to be rendered in the iframe.
   *
   * @return array
   *   The iframe render array.
   */
  public function buildIframe($url) {
    return [
      '#type' => 'inline_template',
      '#template' => '<iframe src="{{ url }}"></iframe>',
      '#context' => [
        'url' => $url,
      ],
    ];
  }

}
