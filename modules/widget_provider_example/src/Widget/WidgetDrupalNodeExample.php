<?php

namespace Drupal\widget_provider_example\Widget;

use Drupal\user\Entity\User;
use Drupal\widget_provider_api\Widget\WidgetBase;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * A Node Widget Example class.
 *
 * It renders a Node as a widget.
 */
class WidgetDrupalNodeExample extends WidgetBase {

  /**
   * The WidgetDrupalNodeExample Constructor.
   */
  public function __construct() {
    parent::__construct('widget-node-example', ['nid'], ['kid']);
  }

  /**
   * {@inheritdoc}
   */
  public function buildContent(): array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $nid = $this->getParameterValue('nid', FALSE);
    $referer = $this->validateAllowedReferer();
    $validate = $this->validateParameters();
    if ($nid === FALSE || $referer === FALSE || $validate === FALSE) {
      // Example for NOT FOUND exception return value:
      return FALSE;
    }
    return $this->buildNode($nid);
  }

  /**
   * Node render function, for creating a node widget.
   *
   * @param string $entity_id
   *   The entity id.
   *
   * @return array
   *   The node render array.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException
   *   Thrown when access is denied.
   */
  public function buildNode($entity_id) {
    $current_user = \Drupal::currentUser();
    $user = User::load($current_user->id());
    $entity = \Drupal::entityTypeManager()->getStorage('node')->load($entity_id);
    if ($entity->access('view', $user)) {
      $view_builder = \Drupal::entityTypeManager()->getViewBuilder('node');
      $pre_render = $view_builder->view($entity);
      return $pre_render;
    }
    throw new AccessDeniedHttpException("You don't have access to this Node!!");
  }

}
