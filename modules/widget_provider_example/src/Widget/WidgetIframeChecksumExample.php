<?php

namespace Drupal\widget_provider_example\Widget;

use Drupal\widget_provider_api\Widget\WidgetBase;

/**
 * A Iframe Widget Example class.
 *
 * It renders an Iframe as a widget.
 */
class WidgetIframeChecksumExample extends WidgetBase {

  /**
   * The WidgetIframeChecksumExample Constructor.
   */
  public function __construct() {
    parent::__construct('widget-checksum-iframe-example', [], [], [], 'testSecret');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    return $this->buildIframe('https://www.google.com/');
  }

  /**
   * {@inheritdoc}
   */
  public function buildContent(): array {
    return [];
  }

  /**
   * Generate a checksum.
   *
   * @return string
   *   The generated checksum.
   */
  public function generateChecksum() {
    $secretID[0] = $this->getValidateChecksumSalt();
    $request = \Drupal::request();
    $time[0] = $request->server->get('REQUEST_TIME');
    $allParameters = $this->getAllParameters();
    $allowedReferrers = $this->getAllowedReferrers();
    $widgetData = array_merge($secretID, $time, $allParameters, $allowedReferrers);
    $widgetDataString = implode('', $widgetData);
    $hash = hash('sha256', $widgetDataString);
    return $hash;
  }

  /**
   * Function for rendering an iframe.
   *
   * @param string $url
   *   The URL to be rendered in the iframe.
   *
   * @return array
   *   The iframe render array.
   */
  public function buildIframe($url) {
    return [
      '#type' => 'inline_template',
      '#template' => '<iframe src="{{ url }}"></iframe>',
      '#context' => [
        'url' => $url,
      ],
    ];
  }

}
