<?php

namespace Drupal\widget_provider_example\Widget;

use Drupal\views\Views;
use Drupal\widget_provider_api\Widget\WidgetBase;

/**
 * A View Widget Example class.
 *
 * It renders a View as a widget.
 */
class WidgetDrupalViewExample extends WidgetBase {

  /**
   * The WidgetDrupalViewExample Constructor.
   */
  public function __construct() {
    parent::__construct('widget-view-example');
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    return $this->buildView('frontpage');
  }

  /**
   * {@inheritdoc}
   */
  public function buildContent(): array {
    return [];
  }

  /**
   * Function for rendering the View.
   *
   * @param string $viewName
   *   The name of the view.
   * @param string $display
   *   The display ID.
   *
   * @return array
   *   The view render array.
   */
  public function buildView($viewName, $display = 'default') {
    $view = Views::getView($viewName);
    if (is_object($view)) {
      $view->setDisplay($display);
      $view->preExecute();
      $view->execute();
      return $view->buildRenderable();
    }
  }

}
