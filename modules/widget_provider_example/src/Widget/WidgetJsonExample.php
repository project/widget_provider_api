<?php

namespace Drupal\widget_provider_example\Widget;

use Drupal\Component\Serialization\Json;
use Drupal\widget_provider_api\Widget\WidgetBase;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * A Javascript Widget Example class.
 *
 * It renders a javascript as a widget.
 */
class WidgetJsonExample extends WidgetBase {

  /**
   * The WidgetJsExample Constructor.
   */
  public function __construct() {
    parent::__construct('widget-js-example', ['kid'], ['hid']);
  }

  /**
   * {@inheritdoc}
   */
  public function buildContent(): array {
    // Nothing here. We use build().
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $nid = $this->getParameterValue('kid', FALSE);
    $referer = $this->validateAllowedReferer();
    $validate = $this->validateParameters();
    if ($nid === FALSE || $referer === FALSE || $validate === FALSE) {
      // Example for NOT FOUND exception return value:
      return FALSE;
    }
    return JsonResponse::fromJsonString(Json::encode(['data' => 'This is an example from JS Example']));
  }

}
