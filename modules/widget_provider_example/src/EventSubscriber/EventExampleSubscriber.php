<?php

namespace Drupal\widget_provider_example\EventSubscriber;

use Drupal\widget_provider_example\Widget\WidgetJsonExample;
use Drupal\widget_provider_api\Widget\DefaultWidgetProvider;
use Drupal\widget_provider_example\Widget\WidgetIFrameExample;
use Drupal\widget_provider_api\Event\RegisterWidgetProvidersEvent;
use Drupal\widget_provider_example\Widget\WidgetDrupalNodeExample;
use Drupal\widget_provider_example\Widget\WidgetDrupalViewExample;
use Drupal\widget_provider_api\EventSubscriber\WidgetProviderEventSubscriber;
use Drupal\widget_provider_example\Widget\WidgetIframeChecksumExample;

/**
 * WidgetProviderAPI event subscriber.
 */
class EventExampleSubscriber extends WidgetProviderEventSubscriber {

  /**
   * Register WidgetExampleProvider to the event.
   *
   * @param \Drupal\widget_provider_api\Event\RegisterWidgetProvidersEvent $event
   *   The event object.
   */
  public function registerWidgetProviders(RegisterWidgetProvidersEvent $event) {
    $event->addWidgetProvider(DefaultWidgetProvider::create('example-provider1', [
      new WidgetDrupalNodeExample(), new WidgetDrupalViewExample(), new WidgetIframeExample(),
    ]));
    $event->addWidgetProvider(DefaultWidgetProvider::create('example-provider2', [
      new WidgetJsonExample(), new WidgetIframeChecksumExample(),
    ]));
    \Drupal::messenger()->addMessage('All Widget Providers registrated!');
  }

}
