<?php

namespace Drupal\Tests\widget_provider_api\Widget;

use Drupal\widget_provider_api\Widget\WidgetBase;

/**
 * A blank widget.
 */
class BlankWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function buildContent(): array {
    return [];
  }

}
