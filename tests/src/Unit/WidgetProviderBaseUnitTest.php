<?php

namespace Drupal\Tests\widget_provider_api\Unit;

use Drupal\Tests\UnitTestCase;
use Drupal\widget_provider_api\Widget\DefaultWidgetProvider;
use Drupal\Tests\widget_provider_api\Widget\BlankWidget;

/**
 * This class provides methods specifically for testing the WidgetProviderBase.
 *
 * @group widget_provider_api
 */
class WidgetProviderBaseUnitTest extends UnitTestCase {
  /**
   * A DefaultWidgetProvider Object.
   *
   * @var Drupal\widget_provider_api\Widget\DefaultWidgetProvider
   */
  protected $widgetProvider;

  /**
   * {@inheritDoc}
   */
  protected function setUp(): void {
    $this->widgetProvider = new DefaultWidgetProvider('test_provider', []);
  }

  /**
   * Tests if it is possible to add a Widget.
   */
  public function testAddWidget() {
    $widget = new BlankWidget('test_widget');
    $addWidget = self::getMethod('addWidget');
    $addWidget->invokeArgs($this->widgetProvider, [$widget]);
    $widgetArray = $this->widgetProvider->getWidgets();
    $this->assertArrayHasKey('test_widget', $widgetArray);
  }

  /**
   * Tests if an unallowed widget id throws an error.
   */
  public function testAddWidgetIdNotAllowed() {
    $this->expectException(\Exception::class);
    $widget = new BlankWidget('&%(/%');
    $addWidget = self::getMethod('addWidget');
    $addWidget->invokeArgs($this->widgetProvider, [$widget]);
  }

  /**
   * Tests if adding multiple of the same widgets throws an error.
   */
  public function testAddWidgetProviderAddMultiple() {
    $this->expectException(\Exception::class);
    $widget = new BlankWidget('test_widget');
    $widget2 = new BlankWidget('test_widget');
    $addWidget = self::getMethod('addWidget');
    $addWidget->invokeArgs($this->widgetProvider, [$widget]);
    $addWidget->invokeArgs($this->widgetProvider, [$widget2]);
  }

  /**
   * Get an accessible DefaultWidgetProvider ReflectionClass method by name.
   *
   * @param string $name
   *   The name of the method.
   */
  protected static function getMethod($name) {
    $class = new \ReflectionClass('\Drupal\widget_provider_api\Widget\DefaultWidgetProvider');
    $method = $class->getMethod($name);
    $method->setAccessible(TRUE);

    return $method;
  }

}
