<?php

namespace Drupal\Tests\widget_provider_api\Unit;

use Drupal\Tests\UnitTestCase;
use Drupal\widget_provider_api\Widget\WidgetApiManager;
use Drupal\widget_provider_api\Widget\DefaultWidgetProvider;

/**
 * This class provides methods specifically for testing the WidgetApiManager.
 *
 * @group widget_provider_api
 */
class WidgetApiManagerUnitTest extends UnitTestCase {

  /**
   * A WidgetApiManager object.
   *
   * @var \Drupal\widget_provider_api\Widget\WidgetApiManager
   */
  protected $widgetApiManager;

  /**
   * Get an accessible WidgetApiManager ReflectionClass method by method name.
   */
  protected static function getMethod($name) {
    $class = new \ReflectionClass('\Drupal\widget_provider_api\Widget\WidgetApiManager');
    $method = $class->getMethod($name);
    $method->setAccessible(TRUE);
    return $method;
  }

  /**
   * {@inheritDoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $eventdispatcher = $this->createMock('Symfony\Component\EventDispatcher\EventDispatcherInterface');
    $this->widgetApiManager = new WidgetApiManager($eventdispatcher);
  }

  /**
   * Tests if addWidgetProvider will add a widgetProvider.
   */
  public function testAddWidgetProvider() {
    $attributeProviders = $this->widgetApiManager->getWidgetProviders();
    if (empty($attributeProviders)) {
      $this->assertTrue(TRUE);
    }
    else {
      $this->assertTrue(FALSE);
    }
    $widgetProvider = $this->createMock('Drupal\widget_provider_api\Widget\WidgetProviderInterface');
    $addWidgetProvider = self::getMethod('addWidgetProvider');
    $addWidgetProvider->invokeArgs($this->widgetApiManager, [$widgetProvider]);
    $attributeProviders = $this->widgetApiManager->getWidgetProviders();
    if (empty($attributeProviders)) {
      $this->assertTrue(FALSE);
    }
    else {
      $this->assertTrue(TRUE);
    }
  }

  /**
   * Tests if an unallowed provider id throws an error.
   */
  public function testAddWidgetProviderIdNotAllowed() {
    $this->expectException(\Exception::class);
    $widgetProvider = new DefaultWidgetProvider('()==)§!', []);
    $addWidgetProvider = self::getMethod('addWidgetProvider');
    $addWidgetProvider->invokeArgs($this->widgetApiManager, [$widgetProvider]);
  }

  /**
   * Tests if adding multiple of the same providers throws an error.
   */
  public function testAddWidgetProviderAddMultiple() {
    $this->expectException(\Exception::class);
    $widgetProvider = new DefaultWidgetProvider('test1', []);
    $widgetProvider2 = new DefaultWidgetProvider('test1', []);
    $addWidgetProvider = self::getMethod('addWidgetProvider');
    $addWidgetProvider->invokeArgs($this->widgetApiManager, [$widgetProvider]);
    $addWidgetProvider->invokeArgs($this->widgetApiManager, [$widgetProvider2]);
  }

}
