<?php

namespace Drupal\widget_provider_api\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\widget_provider_api\Event\RegisterWidgetProvidersEvent;

/**
 * WidgetProviderAPI event subscriber.
 */
abstract class WidgetProviderEventSubscriber implements EventSubscriberInterface {

  /**
   * The widget_provider_example widgetProviders, given to the widget Event.
   *
   * @var \Drupal\widget_provider_api\Widget\WidgetProviderInterface[]
   */
  protected array $widgetProviders;

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      RegisterWidgetProvidersEvent::REGISTER_WIDGET_PROVIDERS => ['registerWidgetProviders'],
    ];
  }

}
