<?php

namespace Drupal\widget_provider_api\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\widget_provider_api\Widget\WidgetApiManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Drupal\widget_provider_api\Exceptions\WidgetNotFoundException;
use Drupal\widget_provider_api\Exceptions\WidgetValidationException;
use Drupal\widget_provider_api\Exceptions\WidgetCreationFailedException;
use Drupal\widget_provider_api\Exceptions\WidgetProviderNotFoundException;
use Drupal\widget_provider_api\Exceptions\NoRegisteredWidgetParameterException;

/**
 * Returns responses for WidgetProviderAPI routes.
 */
class WidgetApiController extends ControllerBase {

  /**
   * A WidgetApiManager Object.
   *
   * @var \Drupal\widget_provider_api\Widget\WidgetApiManager
   */
  protected $widgetApiManager;

  /**
   * Constructs a WidgetApiController object.
   *
   * @param \Drupal\widget_provider_api\Widget\WidgetApiManager $widgetApiManager
   *   The module handler service.
   */
  public function __construct(WidgetApiManager $widgetApiManager) {
    $this->widgetApiManager = $widgetApiManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): object {
    return new static(
      $container->get('widget_provider_api.widget_manager')
    );
  }

  /**
   * Build the response.
   *
   * @param string $providerId
   *   The provider ID.
   * @param string $widgetId
   *   The widget ID.
   *
   * @return array|\Symfony\Component\HttpFoundation\Response
   *   The response array or Response object.
   */
  public function build($providerId, $widgetId) {
    // Catch all exceptions and return a 404 instead:
    try {
      return $this->widgetApiManager->build($providerId, $widgetId);
    }
    catch (
      WidgetCreationFailedException |
      WidgetValidationException |
      WidgetNotFoundException |
      WidgetProviderNotFoundException |
      NoRegisteredWidgetParameterException $e) {
      throw new NotFoundHttpException();
    }
  }

}
