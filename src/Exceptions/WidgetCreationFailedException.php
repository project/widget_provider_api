<?php

namespace Drupal\widget_provider_api\Exceptions;

/**
 * Exception thrown when widget creation fails.
 */
class WidgetCreationFailedException extends \Exception {
}
