<?php

namespace Drupal\widget_provider_api\Exceptions;

/**
 * Exception thrown when the checksum secret is in the wrong format.
 */
class ChecksumSecretWrongFormatException extends \Exception {
}
