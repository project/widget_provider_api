<?php

namespace Drupal\widget_provider_api\Exceptions;

/**
 * Exception for when a widget fails validation.
 */
class WidgetValidationException extends \Exception {

}
