<?php

namespace Drupal\widget_provider_api\Exceptions;

/**
 * Exception thrown when a widget validation has an unallowed referrer.
 */
class WidgetValidationUnallowedReferrerException extends WidgetValidationException {

}
