<?php

namespace Drupal\widget_provider_api\Exceptions;

/**
 * Exception thrown when no registered widget parameter is found.
 */
class NoRegisteredWidgetParameterException extends \Exception {

}
