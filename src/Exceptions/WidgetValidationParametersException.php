<?php

namespace Drupal\widget_provider_api\Exceptions;

/**
 * Exception thrown when widget validation parameters are incorrect.
 */
class WidgetValidationParametersException extends WidgetValidationException {
}
