<?php

namespace Drupal\widget_provider_api\Exceptions;

/**
 * Exception thrown when a widget is not found.
 */
class WidgetNotFoundException extends \Exception {
}
