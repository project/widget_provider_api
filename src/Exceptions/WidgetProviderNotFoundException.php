<?php

namespace Drupal\widget_provider_api\Exceptions;

/**
 * Exception thrown when a widget provider is not found.
 */
class WidgetProviderNotFoundException extends \Exception {
}
