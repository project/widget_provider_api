<?php

namespace Drupal\widget_provider_api\Exceptions;

/**
 * Exception thrown when a widget validation has a wrong checksum.
 */
class WidgetValidationWrongChecksumException extends WidgetValidationException {
}
