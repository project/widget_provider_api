<?php

namespace Drupal\widget_provider_api\Widget;

/**
 * Interface for the Widget Provider.
 */
interface WidgetProviderInterface {

  /**
   * Get WidgetProvider id.
   *
   * @return string
   *   The WidgetProvider id.
   */
  public function getId(): string;

  /**
   * Get Widget Object.
   *
   * @param string $widgetId
   *   The widget id.
   *
   * @return \Drupal\widget_provider_api\Widget\WidgetInterface
   *   The widget object.
   */
  public function getWidget($widgetId): WidgetInterface;

  /**
   * Returns a Widget Object array.
   *
   * @return \Drupal\widget_provider_api\Widget\WidgetInterface[]
   *   An array of widget objects.
   */
  public function getWidgets(): array;

}
