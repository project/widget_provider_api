<?php

namespace Drupal\widget_provider_api\Widget;

use Drupal\Core\Render\BareHtmlPageRenderer;
use Drupal\widget_provider_api\Exceptions\WidgetValidationUnallowedReferrerException;
use Drupal\widget_provider_api\Exceptions\WidgetValidationParametersException;
use Drupal\widget_provider_api\Exceptions\WidgetValidationWrongChecksumException;
use Drupal\widget_provider_api\Exceptions\NoRegisteredWidgetParameterException;
use Drupal\widget_provider_api\Exceptions\ChecksumSecretWrongFormatException;

/**
 * Base class for Widgets.
 */
abstract class WidgetBase implements WidgetInterface {

  /**
   * The Widget ID.
   *
   * @var string
   */
  private $id;

  /**
   * The Widget Specific required Parameters.
   *
   * @var array
   */
  private array $requiredParameters = [];

  /**
   * The Widget Specific allowed Referrers.
   *
   * @var array
   */
  private array $allowedReferrers = [];

  /**
   * A list of all optional parameters.
   *
   * @var array
   */
  private array $optionalParameters = [];

  /**
   * A checksum to verify the User.
   *
   * @var bool|string
   */
  private $validateChecksumSalt;

  /**
   * Factory method Constructor.
   *
   * @param string $id
   *   The widget id.
   * @param array $requiredParameters
   *   The required parameters.
   * @param array $optionalParameters
   *   The optional parameters.
   * @param array $allowedReferrers
   *   The allowed referrers.
   * @param bool|string $validateChecksumSalt
   *   The checksum salt.
   *
   * @return \Drupal\widget_provider_api\Widget\WidgetInterface
   *   The widget interface.
   */
  public static function create($id, array $requiredParameters = [], array $optionalParameters = [], array $allowedReferrers = [], $validateChecksumSalt = FALSE): WidgetInterface {
    return new static($id, $requiredParameters, $optionalParameters, $allowedReferrers, $validateChecksumSalt);
  }

  /**
   * Constructor.
   *
   * @param string $id
   *   The widget id.
   * @param array $requiredParameters
   *   The required parameters.
   * @param array $optionalParameters
   *   The optional parameters.
   * @param array $allowedReferrers
   *   The allowed referrers.
   * @param bool|string $validateChecksumSalt
   *   The checksum salt.
   */
  public function __construct($id, array $requiredParameters = [], array $optionalParameters = [], array $allowedReferrers = [], $validateChecksumSalt = FALSE) {
    if (preg_match('/[^a-z_\-0-9]/i', $id)) {
      throw new \Exception('The widget id "' . $id . '" is invalid, only the following characters are allowed: a-z, 0-9, "-" and "_"');
    }
    $this->id = $id;
    $this->requiredParameters = $requiredParameters;
    $this->optionalParameters = $optionalParameters;
    $this->allowedReferrers = $allowedReferrers;
    $this->validateChecksumSalt = $validateChecksumSalt;
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $buildContent = $this->buildContent();
    if (is_array($buildContent)) {
      // The result was a Drupal render array, we have to render it, otherwise
      // it would be output in site context (template):
      // @todo LATER think about how we could inject the response here and how
      // to configure the expected response type (with or without theme...)
      // @see https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Render%21HtmlResponse.php/class/HtmlResponse/8.2.x
      // Render in bare page without template around:
      $attachments = \Drupal::service('html_response.attachments_processor');
      $renderer = \Drupal::service('renderer');
      $bareHtmlPageRenderer = new BareHtmlPageRenderer($renderer, $attachments);
      // @see https://api.drupal.org/api/drupal/core%21lib%21Drupal%21Core%21Render%21BareHtmlPageRendererInterface.php/interface/BareHtmlPageRendererInterface/8.2.x
      $response = $bareHtmlPageRenderer->renderBarePage($buildContent, $this->getId(), 'markup');
      // @todo This still contains CSS from theme. We have to decide what we really want here and how to control:
      // ALTERNATIVE WITHOUT ANY WRAPPING HTML:
      // $response = new HtmlResponse();
      // // Render RenderArray as response:
      // $response->setContent($buildContent);
      return $response;
    }
    else {
      return $buildContent;
    }
    // }
    throw new WidgetValidationUnallowedReferrerException('You are not allowed to see this Widget!');
  }

  /**
   * Validates all important Parameters.
   *
   * @return bool
   *   TRUE if all parameters are valid, FALSE otherwise.
   */
  public function validateAll() {
    if (!$this->validateAllowedReferer()) {
      throw new WidgetValidationUnallowedReferrerException();
    }
    if (!$this->validateChecksum()) {
      throw new WidgetValidationWrongChecksumException();
    }
    if (!$this->validateParameters()) {
      throw new WidgetValidationParametersException();
    }
    return TRUE;
  }

  /**
   * Validates the Widget Parameters.
   *
   * Checks if all required parameters are present.
   *
   * @return bool
   *   TRUE if all required parameters are present, FALSE otherwise.
   */
  public function validateParameters() {
    $request = \Drupal::request();
    $requiredParameters = $this->getRequiredParameters();
    if (!empty($requiredParameters)) {
      foreach ($requiredParameters as $requiredParameterKey) {
        if (!$request->query->has($requiredParameterKey)) {
          // A required parameter is missing:
          return FALSE;
        }
      }
    }
    // All required parameters are present!
    return TRUE;
  }

  /**
   * Validates the Widget Checksum.
   *
   * @return bool
   *   TRUE if the checksum is valid, FALSE otherwise.
   */
  public function validateChecksum() {
    $salt = $this->getValidateChecksumSalt();
    if ($salt === FALSE) {
      return TRUE;
    }
    $request = \Drupal::request();
    $queryParameters = $this->getAllParameters();
    $queryParameters['ts'] = $request->server->get('REQUEST_TIME');
    if (empty($salt) || $salt === TRUE) {
      throw new ChecksumSecretWrongFormatException();
    }
    ksort($queryParameters);
    $queryParametersSalted = ($salt . implode('', $queryParameters));
    $hash = hash('sha256', $queryParametersSalted);
    if (($request->query->get('s')) == $hash) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Validates the Allowed Referrers of the widget.
   *
   * @return bool
   *   TRUE if the referrer is allowed, FALSE otherwise.
   */
  public function validateAllowedReferer(): bool {
    if (empty($this->allowedReferrers)) {
      return TRUE;
    }
    $request = \Drupal::request();
    $referrerTld = $request->headers->get('referer');
    if (empty($referrerTld)) {
      return TRUE;
    }
    return in_array($referrerTld, $this->allowedReferrers);
  }

  /**
   * {@inheritdoc}
   */
  public function getId(): string {
    return $this->id;
  }

  /**
   * {@inheritdoc}
   */
  public function getAllowedReferrers(): array {
    return $this->allowedReferrers;
  }

  /**
   * {@inheritdoc}
   */
  public function getRequiredParameters(): array {
    if (empty($this->requiredParameters)) {
      throw new \Exception('The widget "' . $this->id . '" has no required Parameters!');
    }
    return $this->requiredParameters;
  }

  /**
   * {@inheritdoc}
   */
  public function getOptionalParameters(): array {
    if (empty($this->optionalParameters)) {
      return [];
    }
    return $this->optionalParameters;
  }

  /**
   * Retrieve the validateChecksumSalt variable.
   *
   * @return bool|string
   *   The checksum secret.
   */
  public function getValidateChecksumSalt() {
    return $this->validateChecksumSalt;
  }

  /**
   * {@inheritdoc}
   */
  public function getAllParameters(): array {
    $requiredParas = $this->getRequiredParameters();
    $optionalParas = $this->getOptionalParameters();
    return array_merge($requiredParas, $optionalParas);
  }

  /**
   * {@inheritdoc}
   */
  public function getParameterValue($parameterKey, $default = NULL) {
    if (!in_array($parameterKey, $this->getAllParameters())) {
      throw new NoRegisteredWidgetParameterException($parameterKey . ' is not a registered widget parameter. You can only retrieve registered required or optional parameters. Did you forget to list them in this widget?');
    }
    $request = \Drupal::request();
    return $request->query->get($parameterKey, $default);
  }

}
