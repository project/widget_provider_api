<?php

namespace Drupal\widget_provider_api\Widget;

use Drupal\widget_provider_api\Exceptions\WidgetNotFoundException;

/**
 * Base class for Widget Providers.
 */
abstract class WidgetProviderBase implements WidgetProviderInterface {

  /**
   * The Widget Provider ID.
   *
   * @var string
   */
  private $id;

  /**
   * Widgets of the provider.
   *
   * @var WidgetInterface[]
   */
  private array $widgets = [];

  /**
   * Static Factory Constructor.
   *
   * @param string $id
   *   The Widget Provider ID.
   * @param WidgetInterface[] $widgets
   *   The Widgets of the provider.
   *
   * @return WidgetProviderInterface
   *   The Widget Provider.
   */
  public static function create(string $id, array $widgets): WidgetProviderInterface {
    return new static($id, $widgets);
  }

  /**
   * Constructor.
   *
   * @param string $id
   *   The Widget Provider ID.
   * @param WidgetInterface[] $widgets
   *   The Widgets of the provider.
   */
  public function __construct(string $id, array $widgets) {
    $this->id = $id;
    if (preg_match('/[^a-z_\-0-9]/i', $id)) {
      throw new \Exception('The WidgetProvider id "' . $id . '" is invalid, only the following characters are allowed: a-z, 0-9, "-" and "_"');
    }
    $this->addWidgets($widgets);
  }

  /**
   * Adds a widget to the list of widgets (indexed by its ID).
   *
   * @param WidgetInterface $widget
   *   The widget to add.
   *
   * @return WidgetProviderInterface
   *   The Widget Provider.
   */
  protected function addWidget(WidgetInterface $widget): WidgetProviderInterface {
    $widgetId = strtolower($widget->getId());
    if (isset($this->widgets[$widgetId])) {
      throw new \Exception('A widget with the ID "' . $widgetId . '" is already registered.');
    }
    $this->widgets[$widgetId] = $widget;
    return $this;
  }

  /**
   * Adds widget providers to the list of widget providers.
   *
   * @param WidgetInterface[] $widgets
   *   The widgets to add.
   *
   * @return WidgetProviderInterface
   *   The Widget Provider.
   */
  protected function addWidgets(array $widgets): WidgetProviderInterface {
    foreach ($widgets as $widget) {
      $this->addWidget($widget);
    }
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getWidgets(): array {
    return $this->widgets;
  }

  /**
   * {@inheritdoc}
   *
   * We should cache this.
   */
  public function getWidget($widgetId): WidgetInterface {
    if (!empty($this->widgets[$widgetId])) {
      return $this->widgets[$widgetId];
    }
    else {
      throw new WidgetNotFoundException($widgetId . ' does not exist.');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getId(): string {
    return $this->id;
  }

}
