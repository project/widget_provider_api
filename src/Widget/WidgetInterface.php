<?php

namespace Drupal\widget_provider_api\Widget;

/**
 * Interface for Widget.
 */
interface WidgetInterface {

  /**
   * Retrieve the Widget id.
   *
   * @return string
   *   The Widget id.
   */
  public function getId(): string;

  /**
   * Retrieve required Parameters.
   *
   * @return array
   *   An array of required parameters.
   */
  public function getRequiredParameters(): array;

  /**
   * Retrieve optional Parameters.
   *
   * @return array
   *   An array of optional parameters.
   */
  public function getOptionalParameters(): array;

  /**
   * Retrieve all Parameters.
   *
   * @return array
   *   An array of all parameters.
   */
  public function getAllParameters(): array;

  /**
   * Retrieve all Allowed Widget Referrers.
   *
   * @return array
   *   An array of allowed referrers.
   */
  public function getAllowedReferrers(): array;

  /**
   * Build the Response content render array.
   *
   * @return array
   *   The response content render array.
   */
  public function buildContent(): array;

  /**
   * Build the Response.
   *
   * @return array|\Symfony\Component\HttpFoundation\Response
   *   The response array or Response object.
   */
  public function build();

  /**
   * Returns the value of the given registered parameter key.
   *
   * @param string $parameterKey
   *   The parameter key.
   * @param mixed $default
   *   The default value if the parameter does not exist.
   *
   * @return mixed
   *   The parameter value.
   *
   * @throws \Drupal\widget_provider_api\Exception\NoRegisteredWidgetParameterException
   *   Thrown when the parameter is not registered.
   */
  public function getParameterValue($parameterKey, $default = NULL);

}
