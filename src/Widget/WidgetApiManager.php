<?php

namespace Drupal\widget_provider_api\Widget;

use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Drupal\widget_provider_api\Event\RegisterWidgetProvidersEvent;
use Drupal\widget_provider_api\Exceptions\WidgetCreationFailedException;
use Drupal\widget_provider_api\Exceptions\WidgetProviderNotFoundException;

/**
 * The Widget API Manager.
 */
class WidgetApiManager {
  /**
   * A hash of available widget Provider indexed by their provider ID.
   *
   * @var WidgetProviderInterface[]
   */
  private array $widgetProviders = [];

  /**
   * Event Dispatcher for dispatching events.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  public function __construct(EventDispatcherInterface $event_dispatcher) {
    $this->eventDispatcher = $event_dispatcher;
  }

  /**
   * Registers all existing providers subscribed to RegisterWidgetProviderEvent.
   */
  private function registerWidgetProviders() {
    $event = RegisterWidgetProvidersEvent::create();
    $this->eventDispatcher->dispatch($event, RegisterWidgetProvidersEvent::REGISTER_WIDGET_PROVIDERS);
    if ($event->hasWidgetProviders()) {
      $this->addWidgetProviders($event->getWidgetProviders());
    }
    else {
      throw new \Exception("No Widget Providers found!");
    }
  }

  /**
   * Adds a widget provider to the list of widget providers (indexed by its ID).
   *
   * @param WidgetProviderInterface $widgetProvider
   *   The widget provider to add.
   *
   * @return WidgetApiManager
   *   The current instance.
   */
  protected function addWidgetProvider(WidgetProviderInterface $widgetProvider): WidgetApiManager {
    $widgetProviderId = $widgetProvider->getId();
    strtolower($widgetProviderId);
    if (preg_match('/[^a-z_\-0-9]/i', $widgetProviderId)) {
      throw new \Exception('The Name of "' . $widgetProviderId . '" is invalid, the following Characters: "a-z, 0-9, _, -" are allowed');
    }
    if (isset($this->widgetProviders[$widgetProviderId])) {
      throw new \Exception('A widget provider with ID "' . $widgetProviderId . '" is already registered.');
    }
    $this->widgetProviders[$widgetProviderId] = $widgetProvider;
    return $this;
  }

  /**
   * Adds widget providers to the list of widget providers.
   *
   * @param WidgetProviderInterface[] $widgetProviders
   *   The widget providers to add.
   *
   * @return WidgetApiManager
   *   The current instance.
   */
  protected function addWidgetProviders(array $widgetProviders): WidgetApiManager {
    foreach ($widgetProviders as $provider) {
      $this->addWidgetProvider($provider);
    }
    return $this;
  }

  /**
   * Get a WidgetProvider Object.
   *
   * @param string $widgetProviderId
   *   The Widget Provider ID.
   *
   * @return WidgetProviderInterface
   *   The Widget Provider.
   *
   * @throws \Drupal\widget_provider_api\Exceptions\WidgetProviderNotFoundException
   */
  public function getWidgetProvider($widgetProviderId): WidgetProviderInterface {
    if (!empty($this->widgetProviders[$widgetProviderId])) {
      return $this->widgetProviders[$widgetProviderId];
    }
    else {
      throw new WidgetProviderNotFoundException($widgetProviderId . ' does not exist.');
    }
  }

  /**
   * Returns the list of WidgetProviderInterface.
   *
   * @return WidgetProviderInterface[]
   *   The list of WidgetProviderInterface.
   */
  public function getWidgetProviders(): array {
    return $this->widgetProviders;
  }

  /**
   * Get a Widget Object.
   *
   * @param string $widgetProviderId
   *   The Widget Provider ID.
   * @param string $widgetId
   *   The Widget ID.
   *
   * @return WidgetInterface
   *   The Widget.
   */
  public function getWidget($widgetProviderId, $widgetId): WidgetInterface {
    return $this->getWidgetProvider($widgetProviderId)->getWidget($widgetId);
  }

  /**
   * Build the Response.
   *
   * @param string $widgetProviderId
   *   The Widget Provider ID.
   * @param string $widgetId
   *   The Widget ID.
   *
   * @return array|Response
   *   The Response.
   */
  public function build($widgetProviderId, $widgetId) {
    // Register all existing providers subscribed to
    // RegisterWidgetProviderEvent:
    $this->registerWidgetProviders();
    $build = $this->getWidget($widgetProviderId, $widgetId)->build();
    if ($build === FALSE) {
      throw new WidgetCreationFailedException();
    }
    return $build;
  }

}
