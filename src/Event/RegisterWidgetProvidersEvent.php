<?php

namespace Drupal\widget_provider_api\Event;

use Symfony\Contracts\EventDispatcher\Event;
use Drupal\widget_provider_api\Widget\WidgetProviderInterface;

/**
 * Event to register widget providers.
 */
class RegisterWidgetProvidersEvent extends Event {
  /**
   * Name of the event fired when a new widget provider is reported.
   *
   * @Event
   *
   * @see \Drupal\widget_provider_api\Event\RegisterWidgetProviderEvent
   *
   * @var string
   */
  const REGISTER_WIDGET_PROVIDERS = 'registerWidgetProviders';

  /**
   * Stores the widgetProviders to register.
   *
   * @var \Drupal\widget_provider_api\Widget\WidgetProviderInterface[]
   */
  protected array $widgetProviders = [];

  /**
   * Constructs an Register event object.
   */
  public function __construct() {
  }

  /**
   * Factory method Constructor.
   *
   * @return \Drupal\widget_provider_api\Event\RegisterWidgetProvidersEvent
   *   The event object.
   */
  public static function create(): RegisterWidgetProvidersEvent {
    return new static();
  }

  /**
   * Checks if a Widget has a WidgetProvider.
   *
   * @return bool
   *   TRUE if there are widget providers, FALSE otherwise.
   */
  public function hasWidgetProviders(): bool {
    return !empty($this->widgetProviders);
  }

  /**
   * Gets the widgetProviders to register.
   *
   * @return \Drupal\widget_provider_api\Widget\WidgetProviderInterface[]
   *   The array of widget providers.
   */
  public function getWidgetProviders() {
    return $this->widgetProviders;
  }

  /**
   * Adds widget provider to the Widget Event List.
   *
   * @param \Drupal\widget_provider_api\Widget\WidgetProviderInterface $widgetProvider
   *   The widget provider to add.
   */
  public function addWidgetProvider(WidgetProviderInterface $widgetProvider) {
    $widgetProviderId = $widgetProvider->getId();
    strtolower($widgetProviderId);
    if (preg_match('/[^a-z_\-0-9]/i', $widgetProviderId)) {
      throw new \Exception('The Name of "' . $widgetProviderId . '" is invalid, the following Characters: "a-z, 0-9, _, -" are allowed');
    }
    if (isset($this->widgetProviders[$widgetProviderId])) {
      throw new \Exception('A widget provider with ID "' . $widgetProviderId . '" is already registered.');
    }
    $this->widgetProviders[$widgetProviderId] = $widgetProvider;
  }

  /**
   * Adds a widget provider to the Widget Event List.
   *
   * @param \Drupal\widget_provider_api\Widget\WidgetProviderInterface[] $widgetProviders
   *   The array of widget providers to add.
   */
  public function addWidgetProviders(array $widgetProviders) {
    foreach ($widgetProviders as $provider) {
      $this->addWidgetProvider($provider);
    }
  }

}
